$(document).ready(function () {
    $(".gallery").slick({
        // Пейджер
        dots: true,
        // Бесконечная прокрутка
        infinite: true,
        // Скорость прокрутки в мс
        speed: 1000,
        // Показывать на экране слайдов (шт)
        slidesToShow: 4,
        // Пролистывать слайдов (шт)
        slidesToScroll: 4,
        // Адаптивность
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
]
    });
});
